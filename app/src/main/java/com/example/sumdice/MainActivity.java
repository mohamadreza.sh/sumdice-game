package com.example.sumdice;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private final static int WIN_SCORE = 100;
    Random random = new Random();
    boolean isPcTurn;
    private TextView txtTurn;
    private ImageView imgDice;
    private TextView txtPcScore;
    private TextView txtHistory;
    private TextView txtUserScore;
    private TextView txtUserHistory;
    private ArrayList<Integer> userNumbers;
    private ArrayList<Integer> pcNumbers;
    private int pcScore;
    private int userScore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeUi();


        userNumbers = new ArrayList<>();
        pcNumbers = new ArrayList<>();


        isPcTurn = random.nextInt(10) >= 5;

        if (isPcTurn) {
            diceDrop();
        }

        processSideCaption();

        imgDice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                diceDrop();
            }
        });

    }

    private void processSideCaption() {
        String turn = "Your turn.Click on dice to continue";

        if (isPcTurn) {
            turn = "Is Pc turn";
        }

        txtTurn.setText(turn);

    }

    private void diceDrop() {
        int number = random.nextInt(6) + 1; //[0,6]

        int imgid = 0;

        switch (number) {
            case 1:
                imgid = R.drawable.dice1;
                break;
            case 2:
                imgid = R.drawable.dice2;
                break;
            case 3:
                imgid = R.drawable.dice3;
                break;
            case 4:
                imgid = R.drawable.dice4;
                break;
            case 5:
                imgid = R.drawable.dice5;
                break;
            case 6:
                imgid = R.drawable.dice6;
                break;
        }

        imgDice.setImageResource(imgid);

        if (isPcTurn) {
            pcNumbers.add(number);
            pcScore += number;
            txtPcScore.setText("" + pcScore);
            txtHistory.setText(computeHistoryString(pcNumbers));

            if (pcScore >= WIN_SCORE) {
                win();
                return;
            }
        } else {
            userNumbers.add(number);
            userScore += number;
            txtUserScore.setText("" + userScore);
            txtUserHistory.setText(computeHistoryString(userNumbers));

            if (userScore >= WIN_SCORE) {
                win();
                return;
            }
        }

        if (number != 6) {
            isPcTurn = !isPcTurn;
            processSideCaption();
        }

        if (isPcTurn) {
            diceDrop();
        }

    }

    private void win() {
        if (isPcTurn) {
            Intent intent = new Intent(MainActivity.this, SecondActivity.class);
            intent.putExtra("RESULT", "PC");
            startActivity(intent);
            finish();
        } else {
            Intent intent2 = new Intent(MainActivity.this, SecondActivity.class);
            intent2.putExtra("RESULT", "USER");
            startActivity(intent2);
            finish();

        }

    }

    private String computeHistoryString(ArrayList<Integer> numbers) {
        String histroy = "";

        boolean isFirst = true;

        for (int i = 0; i < numbers.size(); i++) {
            if (isFirst) {
                histroy += numbers.get(i);
                isFirst = false;
                continue;
            }

            histroy += " + " + numbers.get(i);
        }

        return histroy;
    }


    private void initializeUi() {
        txtTurn = findViewById(R.id.txtTurn);
        imgDice = findViewById(R.id.imgDice);
        txtPcScore = findViewById(R.id.txtPcScore);
        txtHistory = findViewById(R.id.txtHistory);
        txtUserScore = findViewById(R.id.txtUserScore);
        txtUserHistory = findViewById(R.id.txtUserHistory);

        txtPcScore.setText("0");
        txtUserScore.setText("0");
        txtHistory.setText(null);
        txtUserHistory.setText("");


    }

//    private void dice() {
//        int number = (int) Math.floor((Math.random() * 6) + 1);
//        // int rng = random.nextInt(6) + 1; //[0,6]
//        //  txtHistory.setText(new StringBuilder().append(rng));
//        //txtHistory.setText(old+"+"+rng);
//        String old = txtHistory.getText().toString();
//
//        if (!old.isEmpty()) {
//            old += ", ";
//        }
//
//        txtHistory.setText(old + number);
//
//
//        switch (number) {
//            case 1:
//                imgDice.setImageResource(R.drawable.dice1);
//                break;
//            case 2:
//                imgDice.setImageResource(R.drawable.dice2);
//                break;
//            case 3:
//                imgDice.setImageResource(R.drawable.dice3);
//                break;
//            case 4:
//                imgDice.setImageResource(R.drawable.dice4);
//                break;
//            case 5:
//                imgDice.setImageResource(R.drawable.dice5);
//                break;
//            case 6:
//                imgDice.setImageResource(R.drawable.dice6);
//                break;
//
//        }
//
//
//    }

}
